# Web IDE Markdown Preview for Images

This repo was created to showcase that image and markdown preview for image files
does not work in the VSCode Web IDE when the image is stored using
[Git LFS](https://git-lfs.com/).

## Non LFS image

![no_lfs](sun.png)

## LFS image

![lfs](trees.jpg)
